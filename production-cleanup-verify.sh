#!/bin/bash

# This script is meant to be used for verification of files and folders before a deployment to any production environment.
# Run this script after `production-cleanup-run.sh`.
# That way you verify that no testing or development vendor files will be deployed to the production environment.
#
# Usage: ./production-cleanup-verify.sh production-cleanup-locations-definition.txt --verbose

if [[ "$1" = "" ]]; then
    echo "Pass path to file with list of files to check!"
    exit 1
fi

if ! [[ -f "$1" ]]; then
    echo "Could not find file $1"
    exit 2
fi

filesFound=0
filesGone=0
verbose=0

if [[ "$2" = "--verbose" ]]; then
    verbose=1
fi

while IFS= read -r line
do
    if [ -f "$line" -o -d "$line" ]; then
        if [ "$verbose" -eq "1" ]; then
            echo "File $line still exists."
        fi
        filesFound=$((filesFound+1))
    else
        filesGone=$((filesGone+1))
    fi
done < $1

echo "Files found: $filesFound"
echo "Files gone: $filesGone"
