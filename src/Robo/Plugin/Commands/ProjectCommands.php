<?php

namespace Drupal\nrwgov_project\Robo\Plugin\Commands;
use degov\Scripts\Robo\RunsTrait;

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class ProjectCommands extends \Robo\Tasks {

  use RunsTrait;

  /**
   * @command project:new-issue
   * @param string $gitBranchName
   */
  public function projectNewIssue(string $gitBranchName): void {
    $this->newGitBranch($this->rootFolderPath, $gitBranchName, 'remotes/origin/develop');
  }
}
