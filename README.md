# Getting started with nrwGOV development

### Requirements

Der nrwGOV Entwickler-Workflow basiert auf [ddev](https://ddev.readthedocs.io/en/stable/). Bitte lesen Sie die ddev-Dokumentation für die ddev-Anforderungen.

Hier eine kurze Übersicht der (in-)direkt verwendeten Tools:

* [ddev](https://ddev.readthedocs.io/en/stable/)
* [docker](https://docs.docker.com/get-docker/)
* [git](https://git-scm.com/docs)
* [PHP composer](https://getcomposer.org/doc/00-intro.md)
* Ein Editor ihrer Wahl. Für fortgeschrittene Entwickler empfehlen wir [PHP Storm](https://www.jetbrains.com/de-de/phpstorm/).
* Drupal [drush](https://github.com/drush-ops/drush)
* Xdebug für schrittweises Debugging ([using xdebug with ddev](https://ddev.readthedocs.io/en/stable/users/step-debugging/))

Desweiteren verwenden wir folgende Tools, die zumeist über PHP Composer installiert werden:

* [robo](https://robo.li/) - ein Task-Runner, der in Pipeline-Tests und zum Erstellen eines Custom-Theme zum Einsatz kommt
* [behat](https://docs.behat.org/en/v2.5/quick_intro.html) - PHP Test Tool
* [phpunit](https://phpunit.readthedocs.io/) PHP Unit Test Tool
* BackstopJS - Ein Test Tool für Screenshot-basiertes-testen (visual regression testing)


## Ein neues Projekt erstellen

```bash
git clone git@bitbucket.org:publicplan/nrwgov_project.git
```

Jetzt ist eine gute Gelegenheit, das Projekt nach Ihren Wünschen zu benennen. In diesem Beispiel wird es in `nrwgov_project_91` umbenannt:

```bash
mv nrwgov_project nrwgov_project_91
```

**PHP-Abhängigkeiten installieren**

Herunterladen von nrwGOV, deGov, nrw_theme und allen weiteren 'dependencies'.

```bash
cd nrwgov_project_91
composer install
```

### Start der ddev-Entwicklungsumgebung

Ddev verwendet Docker Images, um eine Umgebung zu aufzubauen. Das sind zum Beispiel ein Webserver, ein Datenbankserver und ein Solr (Such-Index) Server.

Es können mehrere ddev Umgebungen (z.B. für verschiedene Projekte oder Versionsstände) gleichzeitig verwaltet werden. Dazu müssen die Port-Einstellungen angepasst werden.

**Ports definieren**

Die folgenden Variablen müssen in `.ddev/config.yaml` angepasst werden. In diesem Beispiel werden wir folgende Variablen verwenden:

```yaml
name: nrwgov91
router_http_port: "8818"
router_https_port: "8819"
```

Nun sollte es klappen das Projekt zu starten:

```bash
cd nrwgov_project_91
ddev start

[...]

Successfully started nrwgov91
Your project can be reached at https://nrwgov91.ddev.site:8819 https://127.0.0.1:60259
```

**Hinweis:**
Um alle ddev-Prozesse zu stoppen und die Resourcen wieder freizugeben: `ddev poweroff`


**Häufiges Problem**

Falls die Ports schon von anderen Prozessen belegt sind, gibt es einen Fehler ähnlich diesem:

```
Failed to start degov:
Unable to listen on required ports, port 80 is already in use
```

In diesem Fall müssen die Ports wie oben beschrieben angepasst werden.
Der Projekt-Name muss auch eindeutig sein. Spätestens beim Start eines zweiten Projekts muss man ihn anpassen.

## nrwGOV installieren

Nach dem Start von ddev kann man die Drupal Installationsseite im Browser unter https://nrwgov91.ddev.site:8819 aufrufen. Das ist ein Drupal-Standard, auf den wir hier nicht im Detail eingehen.

### nrwGOV mit Demo-Content installieren

Für Entwicklung an nrwGOV oder einen schnellen Überblick über die Funktionen empfiehlt sich die Verwendung des Demo Content Moduls (degov_demo_content).

Um einen einen *Entwicklungsstand* zu reproduzieren gibt es ein ddev *custom command* mit dem Namen `kickstart`.
ACHTUNG: ddev kickstart **LÖSCHT ALLE DATEIEN UND DIE DATENBANK** im aktuellen Projekt.

Kurz zusammengefasst macht kickstart folgendes:

* Abhängigkeiten installieren
* local.settings.php anlegen
* Löschen der Datenbank (drush sql-drop)
* Löschen und neu erstellen der Dateien in `sites/default/files`
* Import der aktuellen nrwGOV-Stable-Datenbank.
* Drupal Datenbank- und Locale- (Übersetzungen) Aktualisierungen ausführen

Siehe nrwgov_project_91/.ddev/commands/web/kickstart für weitere Details.

```
cd nrwgov_project_91
# Angenommen ddev start und composer install sind abgeschlossen.
ddev kickstart
```

Nach Abschluss sollte eine Seite mit allen Teasern unter https://nrwgov91.ddev.site:8819 zu sehen sein.

## Login als Admin

Der empfohlene Weg sich einen Admin-Login zu verschaffen ist `drush user-login` ([drush uli](uli)). Mit ddev würde das folgendermaßen aussehen:

```
cd nrwgov_project_91
ddev drush uli
```

## ddev in Kürze

Wir empfehlen die Dokumentation unter [ddev documentation](https://ddev.readthedocs.io/en/stable/) zu lesen. Man kann [drush-Befehle](https://drushcommands.com/drush-9x/) und zahlreiche andere Sachen machen. Einige Beispiele:


```
# Alle ddev Befehle im Projekt anzeigen
ddev

# Übersicht über die ddev Konfiguration des aktuellen Projekts
ddev describe

# Drush Befehle ausführen
ddev drush <drush command>

# Kommandozeilenlogin auf dem (docker) webserver
ddev ssh

# ddev neu starten (z.B. nach einer Änderung der ddev-Konfiguration)
ddev restart

# Ausschalten aller ddev (docker) Dienste
ddev poweroff

# Datenbank mit Sequel Pro (macOS) anzeigen
ddev sequelpro

# Xdebug starten
# See https://ddev.readthedocs.io/en/stable/users/step-debugging
ddev xdebug
```

## Caching und Debugging

Mit ddev kickstart sollten die meisten Caches schon deaktiviert sein.
Wir verwenden dazu Drupal-Standard-Methoden.

Wie das funktioniert und wie Sie sich auch die jeweils verwendeten Twig-Templates im Browser-Inspektor anzeigen lassen können, lesen sie [hier in Kurzform](https://stackoverflow.com/a/33278394/308533).

## Xdebug

Xdebug erlaubt Debugging und Profiling. Damit das funktioniert, muss ggf. die [Entwicklungsumgebung konfiguriert](https://ddev.readthedocs.io/en/stable/users/step-debugging) werden.

Auch Drush-Befehle lassen sich debuggen. Ein Beispiel:

```
ddev xdebug
ddev exec PHP_IDE_CONFIG=serverName=nrwgov91.ddev.site /var/www/html/vendor/drush/drush/drush updb -y
```

## Frontend-Theme

nrwGOV verwendet ein auf Bootstrap basierendes Base-Theme.

Alles rund um das nrwGOV-Base-Theme finden Sie in
[docroot/themes/nrw/nrw_base_theme/README-DE.md](https://bitbucket.org/publicplan/degov_nrw_theme/src/HEAD/README-DE.md)

## Abschließend

Wir - das deGov / nrwGOV Entwickler-Team – machen das hier beschriebene häufig.
Wir lesen nicht das README und sind manchmal blind für Fehler in der Dokumentation und nehmen Dinge für selbstverständlich, von denen andere vielleicht noch nie gehört haben.

**Bitte helfen Sie bei der Verbesserung der Dokumentation**

OpenSource Software lebt vom Mitmachen. Dokumentation zu verbessern ist der einfachste Weg, etwas beizutragen ;)

Source:
[https://bitbucket.org/publicplan/nrwgov_project/src/HEAD/README.md](https://bitbucket.org/publicplan/nrwgov_project/src/HEAD/README.md)
