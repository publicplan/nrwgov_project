<?php

declare(strict_types=1);

use Rector\Core\Configuration\Option;
use Rector\PHPUnit\Set\PHPUnitSetList;
use Rector\Set\ValueObject\SetList;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Rector\Core\ValueObject\PhpVersion;

return static function (ContainerConfigurator $containerConfigurator): void {
  $parameters = $containerConfigurator->parameters();

  // paths to refactor; solid alternative to CLI arguments
  $parameters->set(Option::PATHS, [
    __DIR__ . '/docroot/profiles/contrib/nrwgov',
    __DIR__ . '/docroot/themes/nrw/nrw_base_theme',
  ]);
  $parameters->set(Option::SKIP, [
    __DIR__ . '/docroot/themes/nrw/nrw_base_theme/node_modules',
    __DIR__ . '/docroot/profiles/contrib/nrwgov/settings.platformsh.php',
    \Rector\Php73\Rector\FuncCall\JsonThrowOnErrorRector::class,
    \Rector\PHPUnit\Rector\ClassMethod\AddDoesNotPerformAssertionToNonAssertingTestRector::class
  ]);
  $parameters->set(Option::FILE_EXTENSIONS, ['php', 'module', 'install', 'profile', 'inc', 'theme']);
  // Define what rule sets will be applied
  $parameters->set(Option::SETS, [
    SetList::PHP_53,
    SetList::PHP_54,
    SetList::PHP_55,
    SetList::PHP_56,
    SetList::PHP_70,
    SetList::PHP_71,
    SetList::PHP_72,
    SetList::PHP_73,
    PHPUnitSetList::PHPUNIT_60,
    PHPUnitSetList::PHPUNIT_70,
    PHPUnitSetList::PHPUNIT_75,
    PHPUnitSetList::PHPUNIT_80,
    PHPUnitSetList::PHPUNIT_90,
    PHPUnitSetList::PHPUNIT_91,
    PHPUnitSetList::PHPUNIT_YIELD_DATA_PROVIDER
  ]);
  $parameters->set(Option::IMPORT_SHORT_CLASSES, FALSE);
  $parameters->set(Option::PHP_VERSION_FEATURES, PhpVersion::PHP_73);
  $parameters->set(Option::AUTOLOAD_PATHS, [
    __DIR__ . '/docroot/core/tests/bootstrap.php',
  ]);
  // Run Rector only on changed files
  $parameters->set(Option::ENABLE_CACHE, TRUE);
  // Path to phpstan with extensions, that PHPSTan in Rector uses to determine types
  $parameters->set(Option::PHPSTAN_FOR_RECTOR_PATH, __DIR__. '/phpstan.neon.dist');


  $services = $containerConfigurator->services();
  $services->set(\Rector\CodingStyle\Rector\FuncCall\PreslashSimpleFunctionRector::class);
};
